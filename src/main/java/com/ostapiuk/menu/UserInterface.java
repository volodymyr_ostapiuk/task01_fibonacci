package com.ostapiuk.menu;

import com.ostapiuk.service.FibonacciNumber;
import com.ostapiuk.service.IntervalNumber;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * <p>Class allows user type interval, amount of Fibonacci numbers
 * ans see the result of calculation.</p>
 */
public class UserInterface {
    /**
     * In takes user input from keyboard
     */
    private Scanner in;
    /**
     * {@link IntervalNumber} instance
     */
    private IntervalNumber intervalNumber;
    /**
     * {@link FibonacciNumber} instance
     */
    private FibonacciNumber fibonacciNumber;
    /**
     * Size of array entered by user
     */
    private int amountOfFibonacciNumbers;
    /**
     * Number entered by user
     */
    private int numberInput;

    /**
     * Constructor allows create class.
     */
    public UserInterface() {
        in = new Scanner(System.in, "UTF-8");
    }

    /**
     * Method allows user choose mode and show all calculated information.
     */
    public final void showUserInterface() {
        char exitMode;
        do {
            System.out.println("Welcome to Task01_Fibonacci program!");
            System.out.println("To get started you need "
                    + "to choose number's interval.");
            enterInterval();
            do {
                System.out.println("Select program mode: I/F");
                exitMode = in.next(".").charAt(0);
            } while (exitMode != 'I' && exitMode != 'F');

            if (exitMode == 'I') {
                selectIntervalMode();
            } else if (exitMode == 'F') {
                selectFibonacciMode();
            }
            System.out.println("Quit the program? Press: Y/N");
            exitMode = in.next(".").charAt(0);
        } while (exitMode != 'Y');
    }

    /**
     * Method allows user to enter interval number, helps user to
     * pick numbers from interval [minimumInterval; maximumInterval].
     * Also initialize intervalNumber.
     */
    private void enterInterval() {
        final int maximumInterval = 100;
        final int minimumInterval = 1;
        System.out.print("Please enter the start of interval (>1): ");
        int startOfInterval = enterNumber();
        while (startOfInterval <= minimumInterval
                || startOfInterval >= maximumInterval) {
            System.out.print("Start interval should be > 1 and < 100: ");
            startOfInterval = in.nextInt();
        }

        System.out.print("Please enter the end of interval: ");
        int endOfInterval = enterNumber();
        while (endOfInterval < startOfInterval
                || endOfInterval >= maximumInterval) {
            System.out.print("End interval should be > start and < 100: ");
            endOfInterval = in.nextInt();
        }
        intervalNumber = new IntervalNumber(startOfInterval, endOfInterval);
    }

    /**
     * Method allows to enter only numbers.
     *
     * @return input number by user
     * @throws InputMismatchException
     */
    private int enterNumber() throws InputMismatchException {
        try {
            Scanner input = new Scanner(System.in, "UTF-8");
            numberInput = input.nextInt();
        } catch (InputMismatchException exception) {
            System.out.println(exception);
            System.out.print("Please enter only numbers here: ");
            enterNumber();
        }
        return numberInput;
    }

    /**
     * Method calls IntervalNumber methods to calculate all odd and
     * even numbers, their sum. Also prints the results.
     */
    private void selectIntervalMode() {
        System.out.println("All odd and even numbers from interval:");
        intervalNumber.printNumbers();
        System.out.println("\nCalculated sum of o/e numbers from interval:");
        intervalNumber.printSumOfNumbers();
    }

    /**
     * Method calls IntervalNumber methods to calculate Fibonacci
     * numbers, percentage of odd and even in it. Also prints the results.
     */
    private void selectFibonacciMode() {
        System.out.println("Fibonacci numbers:");
        System.out.print("Please enter the amount of Fibonacci numbers (>2): ");
        enterFibonacciAmount();
        fibonacciNumber = new FibonacciNumber(amountOfFibonacciNumbers,
                intervalNumber);
        fibonacciNumber.printFibonacciNumbers();
        System.out.println("\nPercentage of odd and even Fibonacci numbers");
        fibonacciNumber.printPercentageOfOddEvenFibonacciNumbers();
    }

    /**
     * Method allows user to enter Fibonacci array size,
     * the array should be bigger than minimumAmount.
     */
    private void enterFibonacciAmount() {
        int minimumAmount = 2;
        amountOfFibonacciNumbers = enterNumber();
        while (amountOfFibonacciNumbers < minimumAmount) {
            System.out.print("Amount of Fibonacci numbers should be > 2: ");
            amountOfFibonacciNumbers = in.nextInt();
        }
    }
}
