package com.ostapiuk.service;

/**
 * <p>Class IntervalNumbers displays odd and even  numbers
 * from start to the end of interval,
 * the biggest odd number and the biggest even number,
 * the sum of odd and even numbers. </p>
 */
public class IntervalNumber {
    /**
     * Int array of odd numbers.
     */
    private int[] oddNumbersArray;
    /**
     * Int array of even numbers.
     */
    private int[] evenNumbersArray;
    /**
     * Start of interval.
     */
    private int startOfInterval;
    /**
     * End of interval.
     */
    private int endOfInterval;

    /**
     * Constructor allows create class.
     *
     * @param startInterval start of interval
     * @param endInterval   end of interval
     */
    public IntervalNumber(final int startInterval, final int endInterval) {
        this.startOfInterval = startInterval;
        this.endOfInterval = endInterval;
    }

    /**
     * Method printNumbers allows call print odd and
     * even numbers methods from interval.
     */
    public final void printNumbers() {
        System.out.print("Odd numbers from start ("
                + startOfInterval + ")");
        System.out.println(" to end ("
                + endOfInterval + ") of interval:");
        printOddNumbers();

        System.out.print("\nEven numbers from start ("
                + startOfInterval + ")");
        System.out.println(" to end ("
                + endOfInterval + ") of interval:");
        printEvenNumbers();
    }

    /**
     * Method allows print odd numbers from interval.
     */
    private void printOddNumbers() {
        for (int oddNumber : getOddNumbers()) {
            System.out.print(oddNumber + " ");
        }
    }

    /**
     * Method allows calculate and put in
     * array all odd numbers from interval.
     *
     * @return odd numbers array
     */
    private int[] getOddNumbers() {
        oddNumbersArray = new int[calculateOddNumbersArraySize()];
        for (int i = startOfInterval, j = 0; i <= endOfInterval; i++) {
            if (i % 2 == 1) {
                oddNumbersArray[j++] = i;
            }
        }
        return oddNumbersArray;
    }

    /**
     * Method allows calculate odd numbers array size,
     * finds best way to generate memory for array.
     *
     * @return odd numbers array size
     */
    private int calculateOddNumbersArraySize() {
        int oddNumbersArraySize;
        if ((startOfInterval % 2 == 1) || (endOfInterval % 2 == 1)) {
            oddNumbersArraySize = (endOfInterval - startOfInterval) / 2 + 1;
        } else {
            oddNumbersArraySize = (endOfInterval - startOfInterval) / 2;
        }
        return oddNumbersArraySize;
    }

    /**
     * Method allows print even numbers from interval.
     */
    private void printEvenNumbers() {
        for (int evenNumber : getEvenNumbers()) {
            System.out.print(evenNumber + " ");
        }
    }

    /**
     * Method allows calculate and put in
     * array all even numbers from interval.
     *
     * @return even numbers array
     */
    private int[] getEvenNumbers() {
        evenNumbersArray = new int[calculateEvenNumbersArraySize()];
        for (int i = startOfInterval, j = 0; i <= endOfInterval; i++) {
            if (i % 2 == 0) {
                evenNumbersArray[j++] = i;
            }
        }
        return evenNumbersArray;
    }

    /**
     * Method allows calculate even numbers array size,
     * finds best way to generate memory for array.
     *
     * @return even numbers array size
     */
    private int calculateEvenNumbersArraySize() {
        int evenNumbersArraySize;
        if ((startOfInterval % 2 == 0) || (endOfInterval % 2 == 0)) {
            evenNumbersArraySize = (endOfInterval - startOfInterval) / 2 + 1;
        } else {
            evenNumbersArraySize = (endOfInterval - startOfInterval) / 2;
        }
        return evenNumbersArraySize;
    }

    /**
     * Method allows print sum of odd numbers from interval.
     */
    public final void printSumOfNumbers() {
        System.out.println("Sum of odd numbers equals "
                + calculateSumOfNumbers(oddNumbersArray));
        System.out.println("Sum of even numbers equals "
                + calculateSumOfNumbers(evenNumbersArray));
    }

    /**
     * Method allows calculate sum of odd numbers from interval.
     *
     * @param numbersArray size of array
     * @return sum of numbers
     */
    private int calculateSumOfNumbers(final int[] numbersArray) {
        int sumOfNumbers = 0;
        for (int number : numbersArray) {
            sumOfNumbers += number;
        }
        return sumOfNumbers;
    }

    /**
     * Method allows get biggest odd numbers from interval,
     * it will always be last odd element.
     *
     * @return biggest odd number
     */
    public final int getBiggestOddNumber() {
        if (endOfInterval % 2 != 0) {
            return endOfInterval;
        } else {
            return endOfInterval - 1;
        }
    }

    /**
     * Method allows get biggest even numbers from interval,
     * it will always be last even element.
     *
     * @return biggest even number
     */
    public final int getBiggestEvenNumber() {
        if (endOfInterval % 2 == 0) {
            return endOfInterval;
        } else {
            return endOfInterval - 1;
        }
    }
}
