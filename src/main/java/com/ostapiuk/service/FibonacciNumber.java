package com.ostapiuk.service;

/**
 * <p>Class FibonacciNumbers displays Fibonacci numbers,
 * percentage of odd and even Fibonacci numbers.</p>
 */
public class FibonacciNumber {
    /**
     * Int array of Fibonacci numbers.
     */
    private int[] fibonacciNumbersArray;
    /**
     * {@link IntervalNumber} instance
     */
    private IntervalNumber intervalNumber;
    /**
     * Constant for calculating percentage.
     */
    private static final double PERCENTAGE = 100;

    /**
     * Constructor allows create class.
     *
     * @param fibonacciNumberArraySize size of Fibonacci numbers array
     * @param number                   reference of intervalNumber class
     */
    public FibonacciNumber(final int fibonacciNumberArraySize,
                           final IntervalNumber number) {
        fibonacciNumbersArray = new int[fibonacciNumberArraySize];
        intervalNumber = number;
    }

    /**
     * Method allows print Fibonacci numbers.
     */
    public final void printFibonacciNumbers() {
        for (int fibonacciNumber : buildFibonacciNumbers()) {
            System.out.print(fibonacciNumber + " ");
        }
    }

    /**
     * Method allows calculate and put in
     * array Fibonacci numbers.
     *
     * @return fibonacci numbers array
     */
    private int[] buildFibonacciNumbers() {
        int previousIndex = 1;
        int prePreviousIndex = 2;
        fillFirstTwoFibonacciNumbers();
        for (int i = prePreviousIndex; i < fibonacciNumbersArray.length; i++) {
            fibonacciNumbersArray[i] = fibonacciNumbersArray[i - previousIndex]
                    + fibonacciNumbersArray[i - prePreviousIndex];
        }
        return fibonacciNumbersArray;
    }

    /**
     * Method allows calculate first and second elements
     * from IntervalNumber class.
     */
    private void fillFirstTwoFibonacciNumbers() {
        fibonacciNumbersArray[0] = intervalNumber.getBiggestOddNumber();
        fibonacciNumbersArray[1] = intervalNumber.getBiggestEvenNumber();
    }

    /**
     * Method allows print percentage of
     * odd and even Fibonacci numbers.
     */
    public final void printPercentageOfOddEvenFibonacciNumbers() {
        System.out.println("\nPercentage of odd fibonacci numbers equals "
                + getPercentageOfOddEvenFibonacciNumbers());
        System.out.println("Percentage of even fibonacci numbers equals "
                + (PERCENTAGE - getPercentageOfOddEvenFibonacciNumbers()));
    }

    /**
     * Method allows calculate percentage of
     * odd and even Fibonacci numbers.
     *
     * @return percentage of odd and even Fibonacci numbers
     */
    private double getPercentageOfOddEvenFibonacciNumbers() {
        return ((countAmountOfOddFibonacciNumbers()) * PERCENTAGE)
                / fibonacciNumbersArray.length;
    }

    /**
     * Method allows count the amount of
     * odd Fibonacci numbers.
     *
     * @return amount of odd Fibonacci numbers
     */
    private int countAmountOfOddFibonacciNumbers() {
        int counterOfFibonacciNumber = 0;
        for (int oddEvenFibonacciNumber : fibonacciNumbersArray) {
            if (oddEvenFibonacciNumber % 2 != 0) {
                counterOfFibonacciNumber++;
            }
        }
        return counterOfFibonacciNumber;
    }
}
