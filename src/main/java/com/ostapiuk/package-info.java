/**
 * Copyright (c) Volodymyr Ostapiuk
 * This program allows user to calculate odd,
 * even and Fibonacci numbers from interval.
 */
package com.ostapiuk;
