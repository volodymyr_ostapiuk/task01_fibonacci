package com.ostapiuk;

import com.ostapiuk.menu.UserInterface;

/**
 * <p>Class Main allows user to enter the program.</p>
 *
 * @author Volodymyr Ostapiuk ostap.volodya@gmail.com
 * @version 1.0
 * @since 2019-04-01
 */
public final class Main {
    /**
     * Constructor allows create class.
     */
    private Main() {
    }

    /**
     * Method main allows to start the program.
     *
     * @param args null
     */
    public static void main(final String[] args) {
        UserInterface userInterface = new UserInterface();
        userInterface.showUserInterface();
    }
}
